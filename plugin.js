var exec = require('cordova/exec');

/* constructor */
function VoiceRecognition() {}

VoiceRecognition.prototype.echoTest = function(str, callback) {
console.log('must have been clicked');

        exec(
            function(reply){ callback('ok: '+reply);      },
            function(err){ callback('Error: '+err); }
        , "VoiceRecognition", "echoTest", [str]);
};

VoiceRecognition.prototype.sayIt = function(str, callback) {

        exec(
            function(reply){ callback('ok: '+reply);      },
            function(err){ callback('Error: '+err); }
        , "VoiceRecognition", "sayIt", [str]);
};

VoiceRecognition.prototype.suspendRecognition = function(str, callback) {

        exec(
            function(reply){ callback('ok: '+reply);      },
            function(err){ callback('Error: '+err); }
        , "VoiceRecognition", "suspendRecognition", [str]);
};
VoiceRecognition.prototype.resumeRecognition = function(str, callback) {

        exec(
            function(reply){ callback('ok: '+reply);      },
            function(err){ callback('Error: '+err); }
        , "VoiceRecognition", "resumeRecognition", [str]);
};

VoiceRecognition.prototype.GenerateLanguageModel=function(str,callback){
  exec(
            function(reply){ callback('ok: '+reply);      },
            function(err){ callback('Error: '+err); }
        , "VoiceRecognition", "GenerateLanguageModel",[str]);
};
VoiceRecognition.prototype.startVoiceRecog=function(str,callback){
  exec(
            function(reply){ callback('ok: '+reply);      },
            function(err){ callback('Error: '+err); }
        , "VoiceRecognition", "VoiceRecognition",[str]);
    
};


VoiceRecognition.prototype.backgroundTest = function(str, callback) {
        exec(
            function(reply){ callback('ok: '+reply);      },
            function(err){ callback('Error: '+err); }
        , "Wurst", "backgroundJobTest", [str]);
};

var VoiceRecognition = new VoiceRecognition();
module.exports = VoiceRecognition;