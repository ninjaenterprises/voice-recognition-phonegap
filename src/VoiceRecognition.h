//
//  echo.h
//  VoiceRecog
//
//  Created by Chris Hale on 3/9/14.
//
//



// We need to import this here in order to use the delegate.


/********* Echo.h Cordova Plugin Header *******/

#import <Cordova/CDV.h>
#import <Slt/Slt.h>

#import <OpenEars/PocketsphinxController.h>
#import <OpenEars/AcousticModel.h>

@class PocketsphinxController;
@class FliteController;
#import <OpenEars/OpenEarsEventsObserver.h>

@interface VoiceRecognition : CDVPlugin <OpenEarsEventsObserverDelegate>{
    Slt *slt;
    
    OpenEarsEventsObserver *openEarsEventsObserver; // A class whose delegate methods which will allow us to stay informed of changes in the Flite and Pocketsphinx statuses.
    PocketsphinxController *pocketsphinxController; // The controller for Pocketsphinx (voice recognition).
    FliteController *fliteController; // The controller for Flite (speech).
    
    // Strings which aren't required for OpenEars but which will help us show off the dynamic language features in this sample app.
    BOOL usingStartLanguageModel;
    int restartAttemptsDueToPermissionRequests;
    BOOL startupFailedDueToLackOfPermissions;
    
    NSString *pathToGrammarToStartAppWith;
    NSString *pathToDictionaryToStartAppWith;
    
    NSString *pathToDynamicallyGeneratedGrammar;
    NSString *pathToDynamicallyGeneratedDictionary;
    
    // These three are the important OpenEars objects that this class demonstrates the use of.
}
@property (nonatomic, strong) Slt *slt;

@property (nonatomic, strong) OpenEarsEventsObserver *openEarsEventsObserver;
@property (nonatomic, strong) PocketsphinxController *pocketsphinxController;
@property (nonatomic, strong) FliteController *fliteController;

@property (nonatomic, assign) BOOL usingStartLanguageModel;
@property (nonatomic, assign) int restartAttemptsDueToPermissionRequests;
@property (nonatomic, assign) BOOL startupFailedDueToLackOfPermissions;
// Things which help us show off the dynamic language features.
@property (nonatomic, copy) NSString *pathToGrammarToStartAppWith;
@property (nonatomic, copy) NSString *pathToDictionaryToStartAppWith;
@property (nonatomic, copy) NSString *pathToDynamicallyGeneratedGrammar;
@property (nonatomic, copy) NSString *pathToDynamicallyGeneratedDictionary;


- (void)echoTest:(CDVInvokedUrlCommand*)command;
- (void)sayIt:(CDVInvokedUrlCommand*)command;
- (void)suspendRecognition:(CDVInvokedUrlCommand*)command;
- (void)resumeRecognition:(CDVInvokedUrlCommand*)command;
- (void)VoiceRecognition:(CDVInvokedUrlCommand*)command;
- (void)SuspendVoiceRecognition:(CDVInvokedUrlCommand*)command;
- (void)GenerateLanguageModel:(CDVInvokedUrlCommand*)command;

@end